Taqueria Los Comales offers fresh authentic Mexican Food in a comfortable family environment at an affordable price.
We’ve taken the fast food idea and combined it with the dine-in style comfort to create a “quick-casual” atmosphere in our restaurants.

Address: 9055 N Milwaukee Ave, Niles, IL 60714, USA

Phone: 847-663-0763

Website: https://www.loscomales.com
